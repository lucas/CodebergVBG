    </main>
    <footer>
      <table style="width:100%">
        <tr>
          <td>
            Made by <a href="https://codeberg.org/lucas">@lucas</a>- Inspired by <a href="https://codeberg.org/get-it-on/pages">get-it-on/pages</a><br>
            Logo material is licensed under <a href="http://creativecommons.org/licenses/by/4.0/">CC-BY 4.0</a><br>
            Codeberg and the Codeberg Logo are trademarks of Codeberg e.V
          </td>
          <td style="text-align:right">
            <a href="https://codeberg.org/lucas/CodebergVBG"><img src="?api&subtype=blue-on-white&text=SOURCE+CODE+ON" alt="GetItOnCodeberg" width="120px" height="auto"></a>
          </td>
      </table>
    </footer>
  </body>
</html>